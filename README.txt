This module is used to display the last login time of current logged in user.

This module stores the last login time in a session variable and pass this
variable to Last login time Block. It is a lightweight module which do
not create an extra table in a database.
It is easy to install & use this module.  
        
To display the last login time of current logged in user, You need 
to do the following. 


Installation:
----------------------
1. Download the module and place in sites/all/modules directory.
2. Enable the module in admin/modules.
3. Now goto admin/structure/block.
4. Follow only one of the both ways mentioned below:-
  a). Enable the Last login time block and assign a specific region.
      OR
  b). You can directly print session variable ($_SESSION["user_last_login"])
  in your template file. 
5. You need to logout and login again to see the last login time.
