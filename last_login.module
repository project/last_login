<?php

/**
 * @file
 * Module to display the last login time of current user.
 */

/**
 * Implements hook_help().
 */
function last_login_help($path, $arg) {
  switch ($path) {
    case 'admin/help#last_login':
      $output = '';
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('This module is used to display the last login time for current logged in user.
        It is a lightweight module which do not create an extra table in a database. <br>
        This module stores the last login time in a session variable. <br>
        To display the last login time for current logged in user, You have to go to
        admin/structure/modules and enable the Last login time block by assigning
        a specific region.') . '</p>';

      $output .= '<h3>' . t('Installation:') . '</h3>';
      $output .= '<ol>';
      $output .= '<li>' . t('Download the module and place in sites/all/modules directory.') . '</li>';
      $output .= '<li>' . t('Enable the module in admin/modules.') . '</li>';
      $output .= '<li>' . t('Now goto admin/structure/block.') . '</li>';
      $output .= '<li>' . t('Follow only one of the both ways mention below:-') . '</li>';
      $output .= '<li>' . t('Enable the Last login time block and assign a specific region') . '</li>';
      $output .= '<li>' . t('OR') . '</li>';
      $output .= '<li>' . t('You can directly print session variable
                          (<b> $_SESSION["user_last_login"] </b>) in your template file.') . '</li>';
      $output .= '<li>' . t('You need to logout and login again to see the last login time.') . '</li>';
      $output .= '</ol>';

      return $output;
  }
}

/**
 * Implements hook_form_alter().
 */
function last_login_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'user_login' || $form_id == 'user_login_block') {
    array_unshift($form['#validate'], 'last_login_user_time');
  }
}

/**
 * Callback to fetch last login and store in a session variable.
 */
function last_login_user_time($form, &$form_state) {
  $name = $form_state['values']['name'];
  try {
    $last_login = db_select('users')->fields('users', array(
      'login',
    ))->condition('name', $name)->execute()->fetchField();
    if ($last_login) {
      $_SESSION['user_last_login'] = date('Y-m-d H:i', $last_login);
    }
    else {
      $_SESSION['user_last_login'] = date('Y-m-d H:i', time());
    }
  }
  catch (Exception $e) {
    $msg = t("Unable to execute query, Message: %msg.");
    $vars = array(
      '%msg' => $e->getMessage(),
    );
    watchdog('last_login', $msg, $vars, WATCHDOG_NOTICE);
  }
}

/**
 * Implements hook_block_info().
 */
function last_login_block_info() {
  $blocks = array();
  $blocks['last_login_block'] = array(
    'info' => t('Last login time'),
    'cache' => DRUPAL_NO_CACHE,
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function last_login_block_view($delta = '') {
  $block = array();
  global $user;
  $label = t('Last Login');
  $content = isset($_SESSION['user_last_login']) ? '<div class="last-access">' . $label . ' : ' . $_SESSION['user_last_login'] . '</div>' : '';

  switch ($delta) {
    case 'last_login_block':
      if ($user->uid) {
        $block['content'] = $content;
      }
      else {
        $block['content'] = '';
      }
      break;
  }

  return $block;
}
